export default {
    routes: [{
            path: '/',
            name: 'app',
            component: resolve => require(['./App'], resolve)
        },
        {
            path: '/Square',
            name:'Square',
            component: resolve => require(['./components/Frames'], resolve)
        }, //循环评论面板
        {
            path: '/Friends',
            name:'Friends',
            component: resolve => require(['./components/Frames'], resolve)
        },
        {
            path: '/Commentadd',
            name: 'Add',
            component: resolve => require(['./components/Add'], resolve)
        },
        {
            path: '/App',
            name: 'App',
        },
        {
            path: '/My',
            name: 'My',
        },
    ],
}