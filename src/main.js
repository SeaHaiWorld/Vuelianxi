import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vant from 'vant'
import 'vant/lib/index.css'
import axios from 'axios'
import routerConfig from './router.config'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import index from './index'

Vue.prototype.$http = axios
Vue.use(Vant)
Vue.config.productionTip = false
Vue.use(ElementUI);


Vue.use(VueRouter)
const router = new VueRouter(routerConfig)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')